.PHONY: examples

CC = xelatex
path=$(PWD)
OUT_DIR=build

Anschreibenpdf: 
	
	$(CC) -output-directory=$(path)/$(OUT_DIR) Anschreiben.tex 

cvpdf: 
	$(CC) -output-directory=$(path)/$(OUT_DIR) CurriculumVitae.tex

clean:
	rm -rf $(OUT_DIR)/*.pdf
